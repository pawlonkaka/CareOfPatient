﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using CareOfPatient.Domainn.Concret;
using CareOfPatient.Domainn.Entities;
using System.Threading;


namespace AdminPanel
{


    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ClinicContext context = new ClinicContext();
        public static DataGrid dataGrid;
        public bool isStoped;
        public bool wasClicked;
        CancellationToken cancell = new CancellationToken();
        Task task;

        public MainWindow()
        {
            InitializeComponent();


            wasClicked = false;
            Load();
        }
        
        public void Load()
        {
            myDataGrid.ItemsSource = context.Beds.ToList();
            dataGrid = myDataGrid;
        }

        private void Update_Click(object sender, RoutedEventArgs e)
        {
            int id = (myDataGrid.SelectedItem as Bed).BedID;
            UpdatePage updatepage = new UpdatePage(id);
            updatepage.ShowDialog();
            
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            int id = (myDataGrid.SelectedItem as Bed).BedID;
            var deleteBed = context.Beds.Where(b => b.BedID == id).FirstOrDefault();
            context.Beds.Remove(deleteBed);
            context.SaveChanges();
            myDataGrid.ItemsSource = context.Beds.ToList();
            Load();
        }

        //Generwoanie paramtrów
        public void generetParam(int a, int b,int c , int d, int e, int f)
        {

            Random rnd = new Random();
            var generet = context.Beds.ToList();

            

            foreach (var item in generet)
            {
                item.PressureS = rnd.Next(a, b);
                item.PressureR = rnd.Next(c, d);
                item.Pulse = rnd.Next(e, f);

                if (item.Pulse < 30)
                    item.Status = true;
                else
                    item.Status = false;
                   
            }

            context.SaveChanges();
            myDataGrid.ItemsSource = context.Beds.ToList();
            Load();

        }
        private void refresh_Click(object sender, RoutedEventArgs g)
        {
            int a, b, c, d, e, f;
            a = int.Parse(arg1TextBox.Text);
            b = int.Parse(arg2TextBox.Text);
            c = int.Parse(arg3TextBox.Text);
            d = int.Parse(arg4TextBox.Text);
            e = int.Parse(arg5TextBox.Text);
            f = int.Parse(arg6TextBox.Text);

            generetParam(a, b, c, d, e, f);

        }

        private async void Auto_Click(object sender, RoutedEventArgs e)
        {

            if (wasClicked == false)
            {
                wasClicked = true;
                isStoped = true;
                await Calculate();

            }
            else
            {
                MessageBox.Show("Akcja jest już wykonywana");
            }
            
        }

        private async void Stop_Click(object sender, RoutedEventArgs e)
        {
            if (wasClicked == true)
            {
                await StopingThread();
            }
            else
                MessageBox.Show("Najpierw kliknij START");

        }

        public async Task Calculate()
        {
            task = Task.Run(() =>
            {
                TimeSpan time = new TimeSpan(0, 0, 2);

                int a, b, c, d, e, f;


                while (isStoped)
                {


                    Dispatcher.Invoke(() => generetParam(int.Parse(arg1TextBox.Text),
                                                         int.Parse(arg2TextBox.Text),
                                                         int.Parse(arg3TextBox.Text),
                                                          int.Parse(arg4TextBox.Text),
                                                          int.Parse(arg5TextBox.Text),
                                                          int.Parse(arg6TextBox.Text)));
                    Thread.Sleep(time);
                }
            });

            await task;
        }

        public async Task StopingThread()
        {
            if(wasClicked == true)
            {
                wasClicked = false;
                isStoped = false;

            }

            await task;
        }


    }
}
