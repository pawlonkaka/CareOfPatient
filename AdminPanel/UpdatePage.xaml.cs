﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CareOfPatient.Domainn.Concret;
using CareOfPatient.Domainn.Entities;


namespace AdminPanel
{
    /// <summary>
    /// Logika interakcji dla klasy UpdatePage.xaml
    /// </summary>
    ///

    public partial class UpdatePage : Window
    {
        ClinicContext context = new ClinicContext();
        int ID;


        public UpdatePage(int bedId)
        {
            InitializeComponent();
            this.ID = bedId;
        }

        private void updateBtn_Click(object sender, RoutedEventArgs e)
        {
            Bed updateBed = (from B in context.Beds
                            where B.BedID == ID
                            select B).Single();

            updateBed.Pulse = int.Parse(pulseTextBoxS.Text);

            if (updateBed.Pulse < 30)
                updateBed.Status = true;
            else
                updateBed.Status = false;

            context.SaveChanges();
            MainWindow.dataGrid.ItemsSource = context.Beds.ToList();
            this.Hide();
        }
    }
}
