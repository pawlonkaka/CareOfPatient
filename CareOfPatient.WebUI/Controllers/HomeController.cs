﻿using CareOfPatient.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareOfPatient.Domainn.Entities;
using CareOfPatient.Domainn.Concret;

namespace CareOfPatient.WebUI.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private DataRespository _respository;
        private ClinicContext _context;



        public HomeController()
        {
            _respository = new DataRespository();
            _context = new ClinicContext();
        }

        public ActionResult Index()
        {
            return View(_context.Beds.ToList());
        }

        public ActionResult Detail(int id)
        {
            var list = _context.Beds.ToList();
            var bed = list.SingleOrDefault(m => m.BedID == id);

            return View(bed);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult GetMessage(int? id)
        {


            var list = _respository.GetAllBeds().ToList();
            

            if (id ==null)
                return PartialView("_BedsList", list);
            else
            {

                var bed = list.SingleOrDefault(m => m.BedID == id);

                return PartialView("_Bed", bed);
            }


        }
        public ActionResult BedList()
        {
            return View(_context.Beds.ToList());
        }
        
        public ActionResult AdminPanel(int id)
        {
            var bed = _context.Beds.SingleOrDefault(b => b.BedID == id);

            return View(bed);
        }

        /// <summary>
        /// Update bazy dostępnych łóżek
        /// </summary>
        /// <param name="bed"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(Bed bed)
        {
            
            var recordToEdit = _context.Beds.SingleOrDefault(c => c.BedID == bed.BedID);

            recordToEdit.isFree = bed.isFree;
            
            _context.SaveChanges();

            return RedirectToAction("BedList", "Home");
        }

    }
}