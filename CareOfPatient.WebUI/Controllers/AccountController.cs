﻿using CareOfPatient.WebUI.Infrastructure.Concrete;
using CareOfPatient.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CareOfPatient.WebUI.Controllers
{
    public class AccountController : Controller
    {
        FormsAuthProvider authProvider;

        public AccountController()
        {
            authProvider = new FormsAuthProvider();
        }

        public ViewResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (authProvider.Authenticate(model.UserName, model.Password))
                {
                    return Redirect(returnUrl ?? Url.Action("Index", "Home"));
                }
                else
                {
                    ModelState.AddModelError("", "Nieprawidłowa nazwa użytkownika lub niepoprawne hasło.");
              return View();
                }
            }
            else
            {
                return View();
            }
        }
    }
}