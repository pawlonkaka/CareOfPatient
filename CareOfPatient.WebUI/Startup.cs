﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(CareOfPatient.WebUI.Startup))]

namespace CareOfPatient.WebUI
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            app.MapSignalR();

        }
    }
}
