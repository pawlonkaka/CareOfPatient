﻿using CareOfPatient.Domainn.Entities;
using CareOfPatient.WebUI.Hubs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace CareOfPatient.WebUI.Models
{
    public class DataRespository
    {
        static readonly string _connString = ConfigurationManager.ConnectionStrings["ClinicContext"].ConnectionString;

        public IEnumerable<Bed> GetAllBeds()
        {
            var beds = new List<Bed>();
            using (var connection = new SqlConnection(_connString)) {

                connection.Open();

                using (var command = new SqlCommand(@"SELECT [BedId],[BedName],[Status],[PressureS],[PressureR],[Pulse],[Room] FROM [dbo].[Beds] WHERE [isFree] = '0'", connection))
                {
                    command.Notification = null;

                    var dependency = new SqlDependency(command);
                    dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        

                        beds.Add(item: new Bed
                        {
                            BedID = (int)reader["BedId"],
                            BedName = (BedName)reader["BedName"],
                            Room = reader["Room"] != DBNull.Value ? (string)reader["Room"] : "",
                            PressureS = (int)reader["PressureS"],
                            PressureR = (int)reader["PressureR"],
                            Pulse = (int)reader["Pulse"],
                            Status = (bool)reader["Status"]
                        });

                    }
                }
                return beds;
            }
            
        }



        private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                DataHub.GetData();
            }
        }

       
    }  
}