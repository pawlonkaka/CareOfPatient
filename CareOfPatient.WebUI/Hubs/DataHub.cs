﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Configuration;
using Microsoft.AspNet.SignalR.Hubs;

namespace CareOfPatient.WebUI.Hubs
{
    public class DataHub : Hub
    {
        private static string conString = ConfigurationManager.ConnectionStrings["ClinicContext"].ToString();

        public void Hello()
        {
            Clients.All.hello();
        }

        [HubMethodName("getData")]
        public static void GetData()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<DataHub>();
            context.Clients.All.updateData();
        }


    }
}