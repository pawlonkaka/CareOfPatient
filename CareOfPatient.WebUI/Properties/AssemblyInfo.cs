﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Informacje ogólne o zestawie zależą od poniższego
// zestawu atrybutów. Zmień wartości tych atrybutów, aby zmodyfikować informacje
// związane z zestawem.
[assembly: AssemblyTitle("CareOfPatient.WebUI")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("CareOfPatient.WebUI")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Ustawienie dla atrybutu ComVisible wartości false powoduje, że typy w tym zestawie stają się niewidoczne
// dla składników COM. Jeśli musisz uzyskiwać dostęp do typu w tym zestawie
// z modelu COM, ustaw dla atrybutu ComVisible tego typu wartość true.
[assembly: ComVisible(false)]

// Poniższy identyfikator GUID odpowiada atrybutowi ID biblioteki typów typelib, jeśli ten projekt jest uwidaczniany w modelu COM
[assembly: Guid("b5534f66-e54f-40d6-9b84-83ae2f44af89")]

// Informacje o wersji zestawu obejmują następujące cztery wartości:
//
//      Wersja główna
//      Wersja pomocnicza
//      Numer kompilacji
//      Poprawka
//
// Możesz określić wszystkie te wartości lub użyć wartości domyślnych numerów kompilacji i poprawki,
// stosując znak „*”, jak pokazano poniżej:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
