namespace CareOfPatient.Domainn.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPulseToBedClass : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Beds", "Pulse", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Beds", "Pulse");
        }
    }
}
