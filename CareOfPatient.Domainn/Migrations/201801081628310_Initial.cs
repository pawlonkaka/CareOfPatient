namespace CareOfPatient.Domainn.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Beds",
                c => new
                    {
                        BedID = c.Int(nullable: false, identity: true),
                        BedName = c.Int(nullable: false),
                        Room = c.String(),
                        Pressure = c.Double(nullable: false),
                        Status = c.Boolean(),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.BedID);
            
            CreateTable(
                "dbo.StoredDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        SavedValue = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Login = c.String(),
                        EmailAddress = c.String(),
                        Password = c.String(),
                        Role = c.Int(),
                    })
                .PrimaryKey(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Users");
            DropTable("dbo.StoredDatas");
            DropTable("dbo.Beds");
        }
    }
}
