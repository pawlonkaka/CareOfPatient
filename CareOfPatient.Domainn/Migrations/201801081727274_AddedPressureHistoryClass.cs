namespace CareOfPatient.Domainn.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPressureHistoryClass : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PressureHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Beds", "pressureId", c => c.Int(nullable: false));
            AddColumn("dbo.Beds", "PressureHistory_Id", c => c.Int());
            CreateIndex("dbo.Beds", "PressureHistory_Id");
            AddForeignKey("dbo.Beds", "PressureHistory_Id", "dbo.PressureHistories", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Beds", "PressureHistory_Id", "dbo.PressureHistories");
            DropIndex("dbo.Beds", new[] { "PressureHistory_Id" });
            DropColumn("dbo.Beds", "PressureHistory_Id");
            DropColumn("dbo.Beds", "pressureId");
            DropTable("dbo.PressureHistories");
        }
    }
}
