namespace CareOfPatient.Domainn.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyBedClass : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Beds", "PressureS", c => c.Int(nullable: false));
            AddColumn("dbo.Beds", "PressureR", c => c.Int(nullable: false));
            DropColumn("dbo.Beds", "Pressure");
            DropColumn("dbo.Beds", "Date");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Beds", "Date", c => c.DateTime(nullable: false));
            AddColumn("dbo.Beds", "Pressure", c => c.Double(nullable: false));
            DropColumn("dbo.Beds", "PressureR");
            DropColumn("dbo.Beds", "PressureS");
        }
    }
}
