namespace CareOfPatient.Domainn.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPressureHistoryClass1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Beds", "pressureId");

        }
        
        public override void Down()
        {
            
            
            AddColumn("dbo.Beds", "pressureId", c => c.Int(nullable: false));
        }
    }
}
