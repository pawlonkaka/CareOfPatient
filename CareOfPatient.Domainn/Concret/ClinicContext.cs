﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using CareOfPatient.Domainn.Entities;


namespace CareOfPatient.Domainn.Concret
{
    public class ClinicContext : DbContext
    {

        public DbSet<Bed> Beds { get; set; }
        public DbSet<StoredData> StoredDates { get; set; }
        public DbSet<PressureHistory> PressureHistories { get; set; }

    }
}
