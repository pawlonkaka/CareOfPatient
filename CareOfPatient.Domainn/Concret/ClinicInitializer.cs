﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Threading.Tasks;
using System.Data.Entity;
using CareOfPatient.Domainn.Entities;

namespace CareOfPatient.Domainn.Concret
{
    public class ClinicInitializer : DropCreateDatabaseIfModelChanges<ClinicContext>
    {
        //protected override void Seed(ClinicContext context)
        //{
        //    var users = new List<User>
        //    {
        //        new User(){ FirstName="Alina",  LastName="Balcer", EmailAddress="balceralina@gmail.com", Login="alina",Password="alina1", Role=Role.Employee},
        //        new User(){ FirstName="Kacper",  LastName="Bidon", EmailAddress="bidonkacper@gmail.com", Login="kacper",Password="kacper1", Role=Role.Employee},
        //        new User(){ FirstName="Mateusz",  LastName="Gola", EmailAddress="golamateusz@gmail.com", Login="admin",Password="admin1", Role=Role.Admin},
        //    };

        //    users.ForEach(u => context.Users.Add(u));
        //    context.SaveChanges();

        //    var beds = new List<Bed>
        //    {
        //        new Bed(){ BedName = BedName.A, Room = "1", Pressure = 0.0, Status=false},
        //        new Bed(){ BedName = BedName.B, Room = "1", Pressure = 0.0, Status=false},
        //        new Bed(){ BedName = BedName.C, Room = "1", Pressure = 0.0, Status=false},
        //        new Bed(){ BedName = BedName.D, Room = "1", Pressure = 0.0, Status=false},
        //        new Bed(){ BedName = BedName.E, Room = "1", Pressure = 0.0, Status=false},
        //        new Bed(){ BedName = BedName.F, Room = "1", Pressure = 0.0, Status=false},
        //        new Bed(){ BedName = BedName.A, Room = "2", Pressure = 0.0, Status=false},
        //        new Bed(){ BedName = BedName.B, Room = "2", Pressure = 0.0, Status=false},
        //        new Bed(){ BedName = BedName.C, Room = "2", Pressure = 0.0, Status=false},
        //        new Bed(){ BedName = BedName.D, Room = "2", Pressure = 0.0, Status=false},
        //        new Bed(){ BedName = BedName.E, Room = "2", Pressure = 0.0, Status=false},
        //        new Bed(){ BedName = BedName.F, Room = "2", Pressure = 0.0, Status=false},
        //        new Bed(){ BedName = BedName.A, Room = "3", Pressure = 0.0, Status=false},
        //        new Bed(){ BedName = BedName.B, Room = "3", Pressure = 0.0, Status=false},
        //    };

        //    beds.ForEach(b => context.Beds.Add(b));
        //    context.SaveChanges();
        //}
    }
}
