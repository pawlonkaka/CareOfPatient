﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace CareOfPatient.Domainn.Entities
{
    public class PressureHistory
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public virtual ICollection<int> PressureSCololection { get; set; }
        public virtual ICollection<int> PressureRColection { get; set; }


    }
}