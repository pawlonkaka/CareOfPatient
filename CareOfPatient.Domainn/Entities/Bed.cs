﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CareOfPatient.Domainn.Entities
{
    public enum BedName{
        A,B,C,D,E,F
    }

    public class Bed
    {
        public int BedID { get; set; }
        public BedName BedName { get; set; }
        public string Room { get; set; }
        public int Pulse { get; set; }
        public int PressureS { get; set; }
        public int PressureR { get; set; }
        public bool? Status { get; set; }
        public bool isFree { get; set; }

        public PressureHistory PressureHistory { get; set; }


    }
}
