﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CareOfPatient.Domainn.Entities
{
    public class StoredData
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public float SavedValue { get; set; }
    }
}
